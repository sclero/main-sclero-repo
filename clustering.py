"""
    Copyright (C) 2019  Y. Dogay [dogayyagmur@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.
"""

import scipy.stats as stats
import seaborn as sns
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
from sklearn.cluster import AgglomerativeClustering
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import dendrogram
from fastcluster import linkage
from cleaning_and_selection import clean_mesoscale_data, get_ssc_type


def dendogram_clusters(df):
    """
    clusters obtained from hierarchical clustering
    """
    donor = df['DONOR']
    data = df.loc[:, ~df.columns.isin(['DONOR', 'Diagnosis'])]
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    data[np.isinf(data)] = 0
    data["DONOR"] = donor
    data = data.set_index('DONOR')
    data.rename_axis(None, inplace=True)
    # Calculate the distance between each sample
    # Z = linkage(data, 'ward')

    # plt.title('Hierarchical Clustering Dendrogram')
    # plt.xlabel('Sample Index')
    # plt.ylabel('distance (Ward)')
    # dendrogram(Z, labels=data.index, leaf_rotation=90)
    # plt.axhline(y=40, color='black', linestyle='--')
    # plt.show()
    X = data.iloc[:, :].values
    cluster = AgglomerativeClustering(n_clusters=5, affinity='euclidean', linkage='ward')
    cluster.fit_predict(X)
    list_clusters = list(cluster.labels_)
    data['list_cluster'] = list_clusters
    data = data.sort_values(by=['list_cluster'])
    data['list_cluster'].replace({0: 'Cluster 1', 1: 'Cluster 2', 2: 'Cluster 3', 3: 'Cluster 4', 4: 'Cluster 5'},
                                 inplace=True)
    return data


def t_test(df):
    """
    performed T-tests of cluster data vs other clusters for each cytokine
    """
    clusters = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
    columns = df.columns.values
    i = np.where(columns == 'list_cluster')
    columns = np.delete(columns, i)
    ttest_dataframe = pd.DataFrame()
    for column in columns:
        cluster_list = []
        for c in clusters:
            groups = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
            groups.remove(c)
            this = df[df['list_cluster'] == c]
            this = this.loc[:, ~this.columns.isin(['list_cluster'])]
            other = df[df['list_cluster'].isin(groups)]
            other = other.loc[:, ~other.columns.isin(['list_cluster'])]
            ttest = stats.ttest_ind(this[column], other[column], equal_var=False)
            cluster_list.append(ttest[1])
        ttest_dataframe[column] = cluster_list
    print(ttest_dataframe.min().min())
    print('ttest_data', ttest_dataframe)
    frame_index = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
    ttest_dataframe['frame_index'] = frame_index
    ttest_dataframe = ttest_dataframe.set_index('frame_index')
    ttest_dataframe.to_excel('ttest.xlsx')
    print(ttest_dataframe)
    cols = ttest_dataframe.columns.values
    less_than_005 = []
    for col in cols:
        Index_label = ttest_dataframe[ttest_dataframe[col] < 0.05].index.tolist()
        less_than_005.append((Index_label, col, ttest_dataframe[col].min(), list(ttest_dataframe[[col]].idxmin())))
    print("less_than_005", less_than_005)


def boxplot(df):
    """
    Plotting the distribution of data for each cluster
    """
    columns = df.loc[:, df.columns != 'list_cluster']
    for column in columns:
        plt.figure()
        df.boxplot(column=column, by='list_cluster')
        plt.xlabel('Clusters')
        plt.ylabel(column)
        plt.title("")
        plt.suptitle("")
        path = "/Users/yagmur/Desktop/main-sclero-repo/boxplot/" + column + ".png"
        plt.savefig(path)
        plt.show()


def dendogram_colored(df, df2):
    """
    dendogram of donors based on log scale of cytokines
    """
    donor = df['DONOR']
    data = df.loc[:, ~df.columns.isin(['DONOR', 'Diagnosis'])]
    print(data.head())
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    data[np.isinf(data)] = 0
    print("data", data)
    data["DONOR"] = donor
    data = data.set_index('DONOR')
    print("dataaa", data)
    data.rename_axis(None, inplace=True)
    print(data.shape)

    # Calculate the distance between each sample
    Z = linkage(data, 'ward')
    plt.title('Dendrogram of Donors Hierarchically Clustered \n by Log Transformed Values of Cytokines')
    plt.xlabel('DONOR')
    plt.ylabel('Distance (Ward)')
    palette = plt.cm.get_cmap("Accent", 5)
    dendrogram(Z, labels=data.index, leaf_rotation=90, leaf_font_size=5, color_threshold=40,
               above_threshold_color='grey')
    df2['list_cluster'] = pd.Categorical(df2['list_cluster'])
    color = df2['list_cluster'].cat.codes
    ax = plt.gca()
    xlbls = ax.get_ymajorticklabels()
    num = -1
    for lbl in xlbls:
        num += 1
        val = color[num]
        lbl.set_color(palette(val))
    plt.axhline(y=40, color='black', linestyle='--')
    path = "/Users/yagmur/Desktop/Project Pictures/dendogram.png"
    plt.savefig(path)
    plt.tight_layout()
    plt.show()


def dendogram_with_heatmap(df):
    """
    dendogram  with heatmap of donors based on cytokines
    the average of the cytokine levels from healthy controls
    was divided with the patient data in order to get the ratio
    and then converted to log2 scale.
    """
    data = df.loc[:, ~df.columns.isin(['Diagnosis'])]
    data = data.set_index('DONOR')
    controls = df[df['DONOR'].str.startswith('C')]
    controls = controls.set_index('DONOR')
    controls = controls.drop('Diagnosis', axis=1)
    c_mean = list(controls.mean())
    c_meann = pd.Series(c_mean, index=data.columns)
    data = data.div(c_meann)
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    data[np.isinf(data)] = 0
    g = sns.clustermap(data, cbar_pos=(0.02, 0.85, 0.05, 0.15),
                       cbar_kws={'label': 'Log Fold Value', 'orientation': 'vertical'}, cmap="BrBG", row_cluster=True,
                       col_cluster=True, vmin=-5, vmax=5, yticklabels=1, xticklabels=1)
    g.ax_heatmap.set_xticklabels(g.ax_heatmap.get_xmajorticklabels(), fontsize=5)
    g.ax_heatmap.set_yticklabels(g.ax_heatmap.get_ymajorticklabels(), fontsize=5)
    g.fig.suptitle('Cluster Heatmap of Cytokine Data', fontsize=19)
    path = "/Users/yagmur/Desktop/Project Pictures/clusteredheatmapp.png"
    plt.savefig(path)
    plt.tight_layout()
    plt.show()


def main():
    frame_mesoscale = pd.read_excel("ssc_cohort_studentsv2.xlsx", sheet_name='MesoScale', nrows=94)
    data = clean_mesoscale_data(frame_mesoscale)
    data_frame = get_ssc_type(data)
    dendogram_clusters(data_frame)
    dendogram_with_heatmap(data_frame)
    clustered_data_frame = dendogram_clusters(data_frame)
    dendogram_colored(data_frame, clustered_data_frame)
    t_test(clustered_data_frame)
    boxplot(clustered_data_frame)


if __name__ == '__main__':
    main()
