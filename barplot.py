#!/usr/bin/python3

"""
    Copyright (C) 2019  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import numpy as np
import matplotlib.pyplot as plt


class Barplot:

    def __init__(self, data, width=0.35, color='SkyBlue'):
        self.data = data
        self.ind = np.arange(len(data))  # the x locations for the groups
        self.width = width  # the width of the bars
        self.color = color
        self.fig, self.ax = plt.subplots()

    def plot(self, label, ylabel):
        rects1 = self.ax.bar(self.ind - self.width / 2, self.data, self.width, color=self.color, label=label)

        # Add some text for labels, title and custom x-axis tick labels, etc.
        self.ax.set_ylabel(ylabel)
        self.ax.set_xticks(self.ind)
        return rects1

    def set_title(self, title):
        self.ax.set_title(title)

    def set_label_names(self, names):
        self.ax.set_xticklabels(names)
        self.ax.legend()

    def autolabel(self, rects, xpos='center'):
        """
        Attach a text label above each bar in *rects*, displaying its height.

        *xpos* indicates which side to place the text w.r.t. the center of
        the bar. It can be one of the following {'center', 'right', 'left'}.
        """

        xpos = xpos.lower()  # normalize the case of the parameter
        ha = {'center': 'center', 'right': 'left', 'left': 'right'}
        offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off

        for rect in rects:
            height = rect.get_height()
            self.ax.text(rect.get_x() + rect.get_width() * offset[xpos], 1.01 * height,
                         '{}'.format(height), ha=ha[xpos], va='bottom')
