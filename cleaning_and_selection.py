#!/usr/bin/python3

"""
    Copyright (C) 2020  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import re
import pandas as pd
import numpy as np


def clean_mesoscale_data(df):
    """
    Cleaning the mesoscale data; round the numbers, fill the na and all 0.0 values with the
    lowest value / 2
    :param df: dataframe
    :return: dataframe
    """
    donor = df['DONOR']
    df.replace(0.0, np.nan, inplace=True)
    df.drop('DONOR', axis=1, inplace=True)
    values = df.min()/2
    dict_values = dict(values)
    df = df.fillna(value=dict_values)
    df['DONOR'] = donor
    return df


def get_ssc_type(df):
    """Return a dataframe with the SSc type column added"""
    meta = pd.read_excel(os.path.join(os.getcwd(), "ssc_cohort_studentsv2.xlsx"), sheet_name='Metadata', nrows=376)
    meta = meta[meta['DONOR'].isin(df['DONOR'])]
    donor_selection = [n for n, v in enumerate(meta['Diagnose']) if re.match(r'.*[dl]cssc.*', v, re.IGNORECASE)]
    meta = meta.iloc[donor_selection]
    meta['Diagnose'] = meta['Diagnose'].apply(
        lambda x: 'LcSSc' if re.match(r'.*lcssc.*', x, re.IGNORECASE) else 'DcSSc')
    controls = df[df['DONOR'].str.startswith('C')]
    controls['Diagnosis'] = ['Healthy' for _ in controls['DONOR']]
    df = df[df['DONOR'].isin(meta['DONOR'])]
    df = df.sort_index(axis=1)
    meta = meta.sort_index(axis=1)
    # checking and removing double rows for a single donor
    duplicated_selection = df.duplicated(subset='DONOR', keep='first')
    df = df[[not d for d in duplicated_selection]]
    df['Diagnosis'] = meta[['Diagnose']].values
    df = pd.concat([df, controls], ignore_index=True)
    return df


def get_sex(df):
    """Return a dataframe with the sex of the donor column added"""
    meta = pd.read_excel("ssc_cohort_studentsv2.xlsx", sheet_name='Metadata', nrows=376)
    meta = meta[meta['DONOR'].isin(df['DONOR'])]
    df = df[df['DONOR'].isin(meta['DONOR'])]
    df = df.sort_index(axis=1)
    meta = meta.sort_index(axis=1)
    # checking and removing double rows for a single donor
    duplicated_selection = df.duplicated(subset='DONOR', keep='first')
    df = df[[not d for d in duplicated_selection]]
    g_s = lambda x: int(x) if not np.isnan(x) else 0
    df['Sex'] = [g_s(v) for v in meta[['vrouw']].values]
    return df