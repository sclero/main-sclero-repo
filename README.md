# About this repository #

* This is the repository for the Integrated Omics project.
* Version 1.0

#### How do I get set up to contribute? ####

 First clone the master branch, secondly create a separate branch.
 
#### Dependencies ####

 * The Scipy module
 * The Sklearn module
 * The Seaborn and matplotlib module for visualization
 * The Fastcluster module for clustering data
 * The Pandas module for data manipulation
 * The Openpyxl pandas extension for writing xlsx format
 * The Numpy module for some data manipulation as well as linear algebra functions

#### How to install the modules
  
* `$ python pip3 install scipy --upgrade`
* `$ python pip3 install sklearn --upgrade`
* `$ python pip3 install seaborn --upgrade`
* `$ python pip3 install fastcluster --upgrade`
* `$ python pip3 install pandas --upgrade`
* `$ python pip3 install numpy --upgrade`
* `$ python pip3 install openpyxl --upgrade`

## How to run the application ##

``run any of the clustering.py, pca.py or ttest_and_boxplots.py programs to reproduce results, for the original 
data please contact the admins``

## Contribution guidelines ##

* Please do not merge with master directly but create a side branch.
* Commit your changes to the side branch.
* When you want to add your branch to the project create a pull request.
* Any pull requests will have to be reviewed and approved by a team member.

#### Who do I talk to? ####

* Any pressing questions reach out to admins [rolf.vedder@gmail.com, y.dogay@st.hanze.nl]

![Hanze Logo](https://www.hollandisc.com/-/media/ISC/Holland%20ISC/Images/Hanze%20logo%20160%20x%20400.jpg)