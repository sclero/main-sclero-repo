#!/usr/bin/python3

"""
    Copyright (C) 2020  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import matplotlib.pylab as plt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as stats
from cleaning_and_selection import clean_mesoscale_data, get_ssc_type
from clustering import dendogram_clusters


def t_test(df):
    """Calculates the p values for every cluster vs all the other clusters for each column
    The values are stored in a new dataframe with the columns and clusters"""
    clusters = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
    columns = df.columns.values
    i = np.where(columns == 'list_cluster')
    columns = np.delete(columns, i)
    ttest_dataframe = pd.DataFrame()
    for column in columns:
        cluster_list = []
        for c in clusters:
            groups = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
            groups.remove(c)
            this = df[df['list_cluster'] == c]
            this = this.loc[:, ~this.columns.isin(['list_cluster'])]
            other = df[df['list_cluster'].isin(groups)]
            other = other.loc[:, ~other.columns.isin(['list_cluster'])]
            ttest = stats.ttest_ind(this[column], other[column], equal_var=False)
            cluster_list.append(ttest[1])
        ttest_dataframe[column] = cluster_list
    frame_index = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
    ttest_dataframe['frame_index'] = frame_index
    ttest_dataframe = ttest_dataframe.set_index('frame_index')
    ttest_dataframe.to_excel('ttest.xlsx')
    return ttest_dataframe


def boxplot(df, tt_df):
    """Plots a figure for each cytokine, with boxplots for each cluster and the ttest pvalues for each cluster"""
    frame_index = ['Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5']
    columns = df.loc[:, df.columns != 'list_cluster']
    for column in columns:
        plt.figure()
        ax = df.boxplot(column=column, by='list_cluster', figsize=(9, 6))
        for x, text in zip([1, 2, 3, 4, 5], tt_df[column]):
            y = df[column]
            y = y[df['list_cluster'] == frame_index[x-1]].median()
            ax.text(x - 0.20, y, "p=" + str(round(text, 4)))
        plt.xlabel('Clusters')
        plt.ylabel("Log(level)")
        path = os.path.join(os.getcwd(), "boxplot", column + ".png")
        plt.savefig(path)
        plt.title(column)
        plt.show()


def main():
    frame_mesoscale = pd.read_excel(os.path.join(os.getcwd(), "Cleaning_Data", "ssc_cohort_studentsv2.xlsx"),
                                    sheet_name='MesoScale', nrows=94)
    data = clean_mesoscale_data(frame_mesoscale)
    data_frame = get_ssc_type(data)
    clustered_data_frame = dendogram_clusters(data_frame)
    tt_df = t_test(clustered_data_frame)
    boxplot(clustered_data_frame, tt_df)


if __name__ == '__main__':
    main()




