#!/usr/bin/python3

"""
    Copyright (C) 2019  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# imports
import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt
from barplot import Barplot


def load_antibody_data():
    df = pd.read_excel("ssc_cohort_studentsv2.xlsx", sheet_name='Antibody', nrows=376)
    return df


def replace_val(df):
    colnames = [x for x in df.columns.values]
    new_col = [x for x in colnames if not re.match(r'(.+CT_ELISA)', x)]
    df = df[new_col]
    cuttoffs = {r'.+_EL_.+': 11, r'.+_immcap_SSC': 10, r'ctd-ratio.+': 1, r'.+_DTEK_SSC': 1}
    for pattern, val in cuttoffs.items():
        new_col = [x for x in colnames if re.match(pattern, x)]
        if val == 10:
            new_col = new_col[1:]
        for col in new_col:
            df[col] = df[col].apply(lambda x: 0 if x < val else 1)
    print(df.head(20))
    return df


def clean_antibody_data(df):
    # round numbers into 2 decimals
    df = df.round(4)
    # Apply per-column the min/2 of that columns and fill na values
    df.replace(np.nan, 0.0, inplace=True)
    return df


def get_ssc_type(df):
    """Return a dataframe with the SSc type column added"""
    meta = pd.read_excel("ssc_cohort_studentsv2.xlsx", sheet_name='Metadata', nrows=377)
    meta = meta[meta['DONOR'].isin(df['DONOR'])]
    meta = compare_columns(meta, df, 'DONOR')
    donor_selection = [n for n, v in enumerate(meta['Diagnose']) if re.match(r'.*[dl]cssc.*', v, re.IGNORECASE)]
    meta = meta.iloc[donor_selection]
    meta['Diagnose'] = meta['Diagnose'].apply(lambda x: 'LcSSc' if re.match(r'.*lcssc.*', x, re.IGNORECASE) else 'DcSSc')
    df = compare_columns(df, meta, 'DONOR')
    df = df.sort_index(axis=1)
    meta = meta.sort_index(axis=1)
    duplicated_selection = df.duplicated(subset='DONOR', keep='first')
    duplicated = df[duplicated_selection]
    df = df[[not d for d in duplicated_selection]]
    print('duplicated:\n', duplicated)
    print(df.shape, meta.shape)
    df['Diagnosis'] = meta[['Diagnose']].values
    return df


def compare_columns(df, df1, col):
    return df[df[col].isin(df1[col])]


def do_pca2(df):
    # Separating out the features
    features = list(df.columns.values)
    [features.remove(x) for x in ['DONOR', 'Diagnosis']]
    data = df.loc[:, features].as_matrix()
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    data[np.isinf(data)] = 0
    centered = data
    correlation_matrix = np.dot(centered.T, centered) / (centered.shape[0] - 1)
    eigenvalues, eigenvectors = np.linalg.eig(correlation_matrix)
    order = eigenvalues.argsort()[::-1]
    eigenvalues = eigenvalues[order]
    eigenvectors = eigenvectors[:, order]

    # 'Scree' plot of eigenvalues
    plt.title('Scree plot of eigenvalues')
    plt.plot(eigenvalues)
    plt.savefig('pca_bival/{}{}'.format('scree_antibody_', 'plot'))
    plt.clf()
    # Cumulative percentage of eigenvalues
    barplot = Barplot(np.round(eigenvalues, 2)[0:10])
    bar = barplot.plot('% explained', 'Percentage')
    barplot.set_title('Percentage of variance explained by eigenvalues')
    barplot.set_label_names((x for x in range(1, 11)))
    barplot.autolabel(bar)
    plt.savefig('pca_bival/{}{}'.format('eigenvalues_perc_antibody', 'plot'))
    plt.clf()

    projections = np.dot(centered, eigenvectors)

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlabel('PC 1 {}'.format(np.round(eigenvalues, 2)[0]), fontsize=15)
    ax.set_ylabel('PC 2 {}'.format(np.round(eigenvalues, 2)[1]), fontsize=15)
    ax.set_title('2 component PCA', fontsize=20)
    targets = ['DcSSc', 'LcSSc']
    colors = {k: v for k, v in zip(targets, ['red', 'blue'])}
    for target in targets:
        ind = df['Diagnosis'] == target
        scatt = plt.scatter(projections[ind, 0], projections[ind, 1], c=colors[target], s=5)
        scatt.set_label(target)
    ax.legend()
    ax.grid()
    plt.savefig('pca_bival/{}{}'.format('pca_antibody12_', 'plot'))
    plt.clf()


def remove_single_value_col(df):
    for row in df:
        if df[row].nunique() == 1:
            del df[row]
    return df


def main():
    df = load_antibody_data()
    # df = replace_val(df)
    df = clean_antibody_data(df)
    df = get_ssc_type(df)
    do_pca2(df)
    # df = remove_single_value_col(df)


if __name__ == "__main__":
    main()
