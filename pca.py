#!/usr/bin/python3

"""
    Copyright (C) 2020  R.H.J. Vedder [rolf.vedder@gmail.com]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pylab as plt
from barplot import Barplot
from cleaning_and_selection import *


def cluster(df):
    """Does a knn cluster on the dataframe for the diagnosis"""
    df = df.loc[:, df.columns.values[df.columns.values != 'DONOR']]
    columns = list(df.columns.values)
    [columns.remove(x) for x in ['Diagnosis']]
    data = df.loc[:, columns]
    X = data - data.mean(axis=0)
    y = df['Diagnosis'].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)
    classifier = KNeighborsClassifier(n_neighbors=25)
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test, y_pred))
    error = []

    # Calculating error for K values between 1 and 40
    for i in range(1, 40):
        knn = KNeighborsClassifier(n_neighbors=i)
        knn.fit(X_train, y_train)
        pred_i = knn.predict(X_test)
        error.append(np.mean(pred_i != y_test))
    plt.figure(figsize=(12, 6))
    plt.plot(range(1, 40), error, color='red', linestyle='dashed', marker='o',
             markerfacecolor='blue', markersize=10)
    plt.title('Error Rate K Value')
    plt.xlabel('K Value')
    plt.ylabel('Mean Error')
    plt.show()


def plot_scatter(df):
    """Plots scatter plots for all the Columns, be warned plots every column vs every other column,
    results in a lot of plots."""
    df['color'] = df['Diagnosis'].apply(lambda value: 'red' if value == 'DcSSc' else 'blue')
    columns = list(df.columns.values)
    [columns.remove(x) for x in ['DONOR', 'Diagnosis', 'color']]
    columns2 = list(df.columns.values)
    [columns2.remove(x) for x in ['DONOR', 'Diagnosis', 'color']]
    color = df['color']
    df = df.loc[:, columns]
    df_centered = df - df.mean(axis=0)
    zscores = df_centered / df_centered.std(axis=0)
    for x in columns:
        for y in columns2:
            if x == y:
                continue
            plt.title('{} vs {}'.format(x, y))
            plt.xlabel(x)
            plt.ylabel(y)
            plt.scatter(df_centered[x], df_centered[y], c=color)
            plt.savefig('scatterplots/{}_vs_{}'.format(x, y))
            plt.clf()
        columns2.remove(x)


def do_pca_diagnosis(df):
    """Does a principal component analysis on the mesoscale data and coloured by diagnosis"""
    # Separating out the features
    features = list(df.columns.values)
    [features.remove(x) for x in ['DONOR', 'Diagnosis']]
    data = df.loc[:, features].as_matrix()
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    data[np.isinf(data)] = 0
    centered = data - data.mean(axis=0)
    zscores = centered / centered.std(axis=0)
    correlation_matrix = np.dot(centered.T, centered) / (centered.shape[0] - 1)
    eigenvalues, eigenvectors = np.linalg.eig(correlation_matrix)
    order = eigenvalues.argsort()[::-1]
    eigenvalues = eigenvalues[order]
    eigenvectors = eigenvectors[:, order]
    # 'Scree' plot of eigenvalues
    plt.title('Scree plot of eigenvalues')
    plt.plot(eigenvalues)
    plt.savefig('scatterplots/{}{}'.format('scree_centered_', 'plot'))
    plt.clf()
    # Cumulative percentage of eigenvalues
    barplot = Barplot(np.round(eigenvalues, 2)[0:10])
    bar = barplot.plot('% explained', 'Percentage')
    barplot.set_title('Percentage of variance explained by eigenvalues')
    barplot.set_label_names((x for x in range(1, 11)))
    barplot.autolabel(bar)
    plt.savefig('scatterplots/{}{}'.format('eigenvalues_perc_centered', 'plot'))
    plt.clf()

    projections = np.dot(centered, eigenvectors)
    coeff = np.transpose(projections[0:2, :])
    n = coeff.shape[0]

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylim([-20, 15])
    ax.set_xlim([-15, 15])
    ax.set_xlabel('PC 1 {}'.format(np.round(eigenvalues, 2)[0]), fontsize=14)
    ax.set_ylabel('PC 2 {}'.format(np.round(eigenvalues, 2)[1]), fontsize=14)
    ax.set_title('2 component PCA', fontsize=20)
    targets = ['DcSSc', 'LcSSc', 'Healthy']
    colors = {k: v for k, v in zip(targets, ['red', 'blue', 'black'])}
    df['color'] = df['Diagnosis'].apply(lambda value: colors[value])
    for target in targets:
        ind = df['Diagnosis'] == target
        scatt = plt.scatter(projections[ind, 0], projections[ind, 1], c=colors[target], s=5)
        scatt.set_label(target)
    for i in range(n):
        if abs(coeff[i, 0]) > 0.7:
            plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], color='r', alpha=0.5)
            plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, features[i], color='g', ha='center', va='center')
    ax.legend()
    ax.grid()
    plt.savefig('scatterplots/{}{}'.format('pca_centered_', 'plot'))
    plt.clf()


def do_pca_gender(df):
    """Does a principal component analysis on the mesoscale data and coloured by gender"""
    # Separating out the features
    features = list(df.columns.values)
    [features.remove(x) for x in ['DONOR', 'Sex']]
    data = df.loc[:, features].as_matrix()
    with np.errstate(divide='ignore'):
        data = np.log2(data)
    centered = data - data.mean(axis=0)
    correlation_matrix = np.dot(centered.T, centered) / (centered.shape[0] - 1)
    eigenvalues, eigenvectors = np.linalg.eig(correlation_matrix)
    order = eigenvalues.argsort()[::-1]
    eigenvalues = eigenvalues[order]
    eigenvectors = eigenvectors[:, order]
    # 'Scree' plot of eigenvalues
    plt.title('Scree plot of eigenvalues')
    plt.plot(eigenvalues)
    plt.savefig('scatterplots/{}{}'.format('scree_centered_sex_', 'plot'))
    plt.clf()
    # Cumulative percentage of eigenvalues
    barplot = Barplot(np.round(eigenvalues, 2)[0:10])
    bar = barplot.plot('% explained', 'Percentage')
    barplot.set_title('Percentage of variance explained by eigenvalues')
    barplot.set_label_names((x for x in range(1, 11)))
    barplot.autolabel(bar)
    plt.savefig('scatterplots/{}{}'.format('eigenvalues_perc_centered_sex', 'plot'))
    plt.clf()

    projections = np.dot(centered, eigenvectors)
    coeff = np.transpose(projections[0:2, :])
    n = coeff.shape[0]

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_ylim([-20, 15])
    ax.set_xlim([-15, 15])
    ax.set_xlabel('PC 1 {}'.format(np.round(eigenvalues, 2)[0]), fontsize=14)
    ax.set_ylabel('PC 2 {}'.format(np.round(eigenvalues, 2)[1]), fontsize=14)
    ax.set_title('2 component PCA', fontsize=20)
    targets = [1, 2]
    colors = {k: v for k, v in zip(targets, ['red', 'blue'])}
    df['color'] = df['Sex'].apply(lambda value: colors[value])
    for target in targets:
        ind = df['Sex'] == target
        scatt = plt.scatter(projections[ind, 0], projections[ind, 1], c=colors[target], s=5)
        scatt.set_label(target)
    for i in range(n):
        if abs(coeff[i, 0]) > 0.7:
            plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], color='r', alpha=0.5)
            plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, features[i], color='g', ha='center', va='center')
    ax.legend()
    ax.grid()
    plt.savefig('scatterplots/{}{}'.format('pca_centered_sex', 'plot'))
    plt.clf()


def main():
    frame_mesoscale = pd.read_excel("ssc_cohort_studentsv2.xlsx", sheet_name='MesoScale', nrows=94)
    data_frame = clean_mesoscale_data(frame_mesoscale)
    # plot_scatter(get_ssc_type(data_frame))
    # cluster(get_ssc_type(data_frame))
    do_pca_diagnosis(get_ssc_type(data_frame))
    do_pca_gender(get_sex(data_frame))


if __name__ == '__main__':
    main()
